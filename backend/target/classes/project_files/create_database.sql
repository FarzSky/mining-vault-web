create database mining_vault default character set utf8;
create user 'mining_vault_admin'@'localhost' identified by 'admin123';
grant all on mining_vault.* to 'mining_vault_admin'@'localhost';
