package hr.miningvault.minerapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableAutoConfiguration
@SpringBootApplication
public class MinerAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(MinerAppApplication.class, args);
    }
}
