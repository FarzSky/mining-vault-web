package hr.miningvault.minerapp.database.jpa.repository;

import hr.miningvault.minerapp.model.entity.MinerAcc;
import hr.miningvault.minerapp.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MinerAccRepository extends JpaRepository<MinerAcc, Long> {

    public MinerAcc findByUser(User user);

}
