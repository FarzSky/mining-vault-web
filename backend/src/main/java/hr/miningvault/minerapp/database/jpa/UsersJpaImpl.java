package hr.miningvault.minerapp.database.jpa;

import hr.miningvault.minerapp.database.dao.LoginDao;
import hr.miningvault.minerapp.database.jpa.repository.MinerAccRepository;
import hr.miningvault.minerapp.database.jpa.repository.UsersRepository;
import hr.miningvault.minerapp.model.dto.LoginResponseDto;
import hr.miningvault.minerapp.model.entity.MinerAcc;
import hr.miningvault.minerapp.model.entity.User;
import hr.miningvault.minerapp.model.enumeration.SuccessStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public class UsersJpaImpl implements LoginDao {

    private final UsersRepository userRepository;
    private final MinerAccRepository minerAccRepository;

    @Autowired
    public UsersJpaImpl(UsersRepository loginRepository, MinerAccRepository minerAccRepository) {
        this.userRepository = loginRepository;
        this.minerAccRepository = minerAccRepository;
    }

    @Override
    public LoginResponseDto login(String username, String password) {
        User user = userRepository.findByUsernameAndPassword(username, password);

        LoginResponseDto loginResponseDto = new LoginResponseDto();

        if (user == null) {
            loginResponseDto.setLoginStatus(SuccessStatus.FAILED.name());
        } else {

            MinerAcc minerAcc = minerAccRepository.findByUser(user);

            if (minerAcc == null) {
                loginResponseDto.setLoginStatus(SuccessStatus.FAILED.name());
            } else {
                loginResponseDto.setMinerId(minerAcc.getMinerId());
                loginResponseDto.setUsername(user.getUsername());
                loginResponseDto.setLoginStatus(SuccessStatus.SUCCESS.name());
            }
        }

        return loginResponseDto;
    }

}

