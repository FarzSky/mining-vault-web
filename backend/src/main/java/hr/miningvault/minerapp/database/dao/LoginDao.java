package hr.miningvault.minerapp.database.dao;

import hr.miningvault.minerapp.model.dto.LoginResponseDto;

public interface LoginDao {

    LoginResponseDto login(String username, String password);

}
