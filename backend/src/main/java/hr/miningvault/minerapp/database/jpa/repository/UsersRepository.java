package hr.miningvault.minerapp.database.jpa.repository;

import hr.miningvault.minerapp.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<User, String> {

    User findByUsernameAndPassword(String username, String password);

}
