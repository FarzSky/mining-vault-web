package hr.miningvault.minerapp.controller;

import hr.miningvault.minerapp.model.dto.BasicUserDto;
import hr.miningvault.minerapp.model.dto.LoginResponseDto;
import hr.miningvault.minerapp.model.dto.RestDto;
import hr.miningvault.minerapp.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class LoginController {

    private final LoginService loginService;

    @Autowired
    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @PostMapping("/login")
    public RestDto<LoginResponseDto> login(@RequestBody @Valid BasicUserDto basicUserDto) {
        return RestDto.success(loginService.login(basicUserDto.getUsername(), basicUserDto.getPassword()), "Success");
    }

}
