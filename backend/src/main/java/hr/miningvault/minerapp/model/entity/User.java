package hr.miningvault.minerapp.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "users")
public class User {

    @Id
    @Column(name = "username", nullable = false)
    @Size(min = 0, max = 32)
    private String username;

    @Column(name = "password", nullable = false)
    @Size(min = 6, max = 256)
    private String password;

    @Column(name = "email", nullable = false)
    @Pattern(regexp = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\."
            +"[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
            +"(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message = "{invalid.email}")
    @Size(max = 64)
    private String email;

    @Column(name = "registered_from", nullable = false)
    private Date registeredFrom;

    public User() {
    }

    public User(String username, String password, @Pattern(regexp = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\."
            + "[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
            + "(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?") String email, Date registeredFrom) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.registeredFrom = registeredFrom;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getRegisteredFrom() {
        return registeredFrom;
    }

    public void setRegisteredFrom(Date registeredFrom) {
        this.registeredFrom = registeredFrom;
    }

}
