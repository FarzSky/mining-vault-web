package hr.miningvault.minerapp.model.dto;

import lombok.Data;

@Data
public class BasicUserDto {

    private String username;
    private String password;

}
