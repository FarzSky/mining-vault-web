package hr.miningvault.minerapp.model.entity;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "miner_acc")
public class MinerAcc {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne
    @JoinColumn(name = "username")
    private User user;

    @Column(name = "miner_id", nullable = false)
    @Size(max = 128)
    private String minerId;

    public MinerAcc() {
    }

    public MinerAcc(User user, @Size(max = 128) String minerId) {
        this.user = user;
        this.minerId = minerId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getMinerId() {
        return minerId;
    }

    public void setMinerId(String minerId) {
        this.minerId = minerId;
    }
}
