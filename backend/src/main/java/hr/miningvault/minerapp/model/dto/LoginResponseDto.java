package hr.miningvault.minerapp.model.dto;

import lombok.Data;

@Data
public class LoginResponseDto {

    private String username;
    private String minerId;
    private String loginStatus;

}
