package hr.miningvault.minerapp.service;

import hr.miningvault.minerapp.database.dao.LoginDao;
import hr.miningvault.minerapp.model.dto.LoginResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {

    private final LoginDao loginDao;

    @Autowired
    public LoginService(LoginDao loginDao) {
        this.loginDao = loginDao;
    }

    public LoginResponseDto login(String username, String password) {
        return loginDao.login(username, password);
    }

}
